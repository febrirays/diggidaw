package com.hackathon.rocky.service;

import com.hackathon.rocky.dto.CouponDto;
import com.hackathon.rocky.entity.Coupon;
import com.hackathon.rocky.entity.CouponTransaction;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface CouponService {

    Coupon saveCoupon(CouponDto couponDto);
    void updateCoupon(Coupon coupon);
    void deleteCoupon(String coupon);
    Coupon getCouponById(String couponid);
    CouponDto getcouponDTO(String couponid);
    void saveImage(String couponId,MultipartFile file);
    List<Coupon> getAllData();

    List<CouponDto> getAllCouponDto();
//    void getCoupon(String coupon);



}
