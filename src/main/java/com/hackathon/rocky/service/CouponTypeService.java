package com.hackathon.rocky.service;

import com.hackathon.rocky.entity.CouponType;

public interface CouponTypeService {

    void saveCouponType(CouponType couponType);
    void UpdateCouponType(CouponType couponType);
    void DeleteCouponType (String couponType);
}
