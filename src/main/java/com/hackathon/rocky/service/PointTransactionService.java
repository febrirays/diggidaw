package com.hackathon.rocky.service;

import com.hackathon.rocky.dto.PointTransactionDto;
import com.hackathon.rocky.entity.PointTransaction;

import java.util.List;

public interface PointTransactionService {
    void savePointTransaction(PointTransaction pointTransaction);
    List<PointTransaction> getAllPointTransaction();
    List<PointTransaction> getPointTransactionByUserId(String userID);
    List<PointTransactionDto> getPointTransactionByUserId_dto(String userID);
}
