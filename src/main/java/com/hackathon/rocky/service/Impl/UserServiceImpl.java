package com.hackathon.rocky.service.Impl;

import com.hackathon.rocky.dao.CouponRepository;
import com.hackathon.rocky.dao.UserRepository;
import com.hackathon.rocky.dto.UserDto;
import com.hackathon.rocky.entity.ContractTransaction;
import com.hackathon.rocky.entity.User;
import com.hackathon.rocky.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;


    private UserDto userDto;

    public UserServiceImpl(UserRepository userRepository){this.userRepository = userRepository;}


    //ini update
//    @Override
//    public User saveUser(UserDto userDto){
//        User user = userRepository.getOne(userDto.User_id);
//        user.setUser_name(userDto.getUser_name());
////        userDto.setUser_id();
////        User user = new User();
//        userRepository.save(user);
//        return user;
//    }

    @Override
    public void saveUser(User user){
        userRepository.save(user);
    }

    @Override
    public List<User> findAll() {

    return userRepository.findAll();
    }
}
