package com.hackathon.rocky.service.Impl;


import com.hackathon.rocky.dao.ContractTransactionRepository;
import com.hackathon.rocky.dao.UserRepository;
import com.hackathon.rocky.dto.ContractTransactionDto;
import com.hackathon.rocky.entity.ContractTransaction;
import com.hackathon.rocky.entity.User;
import com.hackathon.rocky.service.ContractTransService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ContractTransServiceImpl implements ContractTransService {

    @Autowired
    private ContractTransactionRepository contractTransactionRepository;

    @Autowired
    private UserRepository userRepository;
    public ContractTransServiceImpl(ContractTransactionRepository contractTransactionRepository){this.contractTransactionRepository = contractTransactionRepository;}



    @Override
    public void savecontract(ContractTransactionDto contractTransactionDto){

        User user = userRepository.getOne(contractTransactionDto.getUser_id());

        ContractTransaction contractTransaction= new ContractTransaction();
        contractTransaction.setTenor_price(contractTransactionDto.getTenor_price());
        contractTransaction.setTotal_cicilan(contractTransactionDto.getTotal_cicilan());
        contractTransaction.setUser(user);

        contractTransactionRepository.save(contractTransaction);
    }

    @Override
    public List<ContractTransaction> findAll(String userId) {
       return contractTransactionRepository.findTransactionByUserId(userId);
    }

    @Override
    public List<ContractTransactionDto> showhistory(String userId) {
        List<ContractTransactionDto> contractTransactionDtos = new ArrayList<>();
        ContractTransactionDto contractTransactionDto= new ContractTransactionDto();

        List<ContractTransaction> contractTransactions= contractTransactionRepository.findTransactionByUserId(userId);

        for (ContractTransaction transaction : contractTransactions){
            contractTransactionDto.setTenor_price(transaction.getTenor_price());
            contractTransactionDto.setTotal_cicilan(transaction.getTotal_cicilan());
            contractTransactionDto.setUser_name(transaction.getUser().getUser_name());
            contractTransactionDtos.add(contractTransactionDto);
        }
        return contractTransactionDtos;


    }

}
