package com.hackathon.rocky.service.Impl;

import com.hackathon.rocky.dao.ARLocationRepository;
import com.hackathon.rocky.entity.ARLocation;
import com.hackathon.rocky.service.ARLocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ARLocationServiceImpl implements ARLocationService {

    @Autowired
    private ARLocationRepository arLocationRepository;

    @Override
    public void saveARLocation(ARLocation arLocation) {
        arLocationRepository.save(arLocation);
    }

    @Override
    public void updateARLocation(ARLocation arLocation) {
        arLocationRepository.save(arLocation);
    }

    @Override
    public void deleteARLocation(String arLocationId) {
        arLocationRepository.delete(arLocationId);

    }
}
