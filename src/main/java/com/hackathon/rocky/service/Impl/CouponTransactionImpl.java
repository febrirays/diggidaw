package com.hackathon.rocky.service.Impl;


import com.hackathon.rocky.dao.CouponRepository;
import com.hackathon.rocky.dao.CouponTransactionRepository;
import com.hackathon.rocky.dao.UserRepository;
import com.hackathon.rocky.dto.CouponRequest;
import com.hackathon.rocky.dto.CouponTransactionDto;
import com.hackathon.rocky.dto.ResponseDTO;
import com.hackathon.rocky.dto.UserHistoryDto;
import com.hackathon.rocky.entity.*;
import com.hackathon.rocky.service.CouponTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class CouponTransactionImpl implements CouponTransactionService {




    @Autowired
    private CouponTransactionRepository couponTransactionRepository;

    @Autowired
    private CouponRepository couponRepository;

    @Autowired
    private UserRepository userRepository;

    public CouponTransactionImpl(CouponTransactionRepository couponTransactionRepository) {
        this.couponTransactionRepository = couponTransactionRepository;
    }


//    @Override
//    public CouponTransaction UserHistory(CouponRequest couponRequest) {
//       CouponTransaction couponTransaction = couponTransactionRepository.getOne(couponRequest.getUserId());
//        //CouponTransaction couponTransaction = new CouponTransaction();
//
//        //couponTransaction.setUser(couponTransactionRepository.getOne(couponRequest.getUserId()));
//
//        couponTransaction
//
//
//        return null;
//    }



//    @Override
//    public CouponTransaction findByAllTransactionByUserID(CouponTransactionDto couponTransactionDto) {
//
//        User user = userRepository.getOne(couponTransactionDto.getUserId());
//
//        CouponTransaction couponTransaction = new CouponTransaction();
//        couponTransaction.setUser(user);
//
//        couponTransactionRepository.findOne(List<CouponTransaction> couponTransaction)
//
//        return null;
//    }

    //TODO buat proses count total token

    @Override
    public ResponseDTO findAllTransactionByUserID(String userId) {
        ResponseDTO responseDTO = new ResponseDTO();
        List<UserHistoryDto> userHistoryDtos = new ArrayList<>();


        List<CouponTransaction> couponTransactions = couponTransactionRepository.findTransactionByUserId(userId);
        User user = new User();

        //TODO handshake : gambar, nama coupon, desc, date(d    dmmyy),coup price
        //TODO buat sehingga penarikan history gk bisa diakses semua orang
        //TODO buat sehingga get.userpoint gak looping
        for(CouponTransaction couponTransaction : couponTransactions){
            responseDTO.setUserPoint(couponTransaction.getUser().getUser_point()); //Get (looping) user-point
            UserHistoryDto userHistoryDto = new UserHistoryDto();


            String[] dateSplit = couponTransaction.getCoupon_transtime().split("T");

            //get all needed data to show to front end
            userHistoryDto.setTransactionId(couponTransaction.getCoupon_trans_id());
            userHistoryDto.setCouponName(couponTransaction.getCoupon().getCoupon_name());
            userHistoryDto.setPointReduced(couponTransaction.getPoints_reduced());
            userHistoryDto.setCouponDesc(couponTransaction.getCoupon().getCoupon_desc());
            userHistoryDto.setDate(dateSplit[0]);


            //userHistoryDto.setCouponPic(couponTransaction.getCoupon().getCoupon_picture());

            userHistoryDtos.add(userHistoryDto);
        }
        responseDTO.setUserHistories(userHistoryDtos);

        return responseDTO;
    }

    //TODO Front end nge kirim data ke backend apa aja??

    //Input transaksi baru, sekaligus konekin antar user dan kupon
    public CouponTransaction saveCouponTrans(CouponRequest couponRequest) {



        Coupon coupon = couponRepository.getOne(couponRequest.getCouponId());
        User user = userRepository.getOne(couponRequest.getUserId());

        //nge set current point dan point user setelah transaksi
        CouponTransaction couponTransaction = new CouponTransaction();
        couponTransaction.setCurrent_point(user.getUser_point() - coupon.getCoupon_price());
        user.setUser_point(user.getUser_point() - coupon.getCoupon_price());
        couponTransaction.setPoints_reduced(coupon.getCoupon_price());

        couponTransaction.setCoupon(coupon); //re learn this
        couponTransaction.setUser(user);



        //buat jadi ngurangin currentpoint

        couponTransactionRepository.save(couponTransaction);
        return couponTransaction ;
    }

//    public void  coupon_redeem(){
//            saveCouponTrans( );
//    }

    public void updateCouponTrans(CouponTransaction couponTransaction) {
        couponTransactionRepository.save(couponTransaction);
    }

    public void deleteCouponTrans(String couponTransaction) {
        couponTransactionRepository.delete(couponTransaction);
    }


    @Override
    public  List<CouponTransaction> getAllData() {
       return couponTransactionRepository.findAll();
    }





}