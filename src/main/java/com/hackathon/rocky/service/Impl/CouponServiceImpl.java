package com.hackathon.rocky.service.Impl;

import com.hackathon.rocky.controller.CouponController;
import com.hackathon.rocky.dao.CouponRepository;
import com.hackathon.rocky.dao.CouponTypeRepository;
import com.hackathon.rocky.dao.PartnerRepository;
import com.hackathon.rocky.dto.CouponDto;
import com.hackathon.rocky.entity.Coupon;
import com.hackathon.rocky.entity.CouponTransaction;
import com.hackathon.rocky.entity.CouponType;
import com.hackathon.rocky.entity.Partner;
import com.hackathon.rocky.service.CouponService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Service
public class CouponServiceImpl implements CouponService {

    Logger log = LoggerFactory.getLogger(CouponServiceImpl.class);

    @Autowired
    private CouponRepository couponRepository;

    @Autowired
    private CouponTypeRepository couponTypeRepository;

    @Autowired
    private PartnerRepository partnerRepository;

    public CouponServiceImpl(CouponRepository CouponRepository){this.couponRepository = CouponRepository;}


//    public void getCoupon(Coupon coupon) {
////        couponRepository.findAll();
//    }




    public Coupon saveCoupon(CouponDto couponDto) {
        Partner partner = partnerRepository.getOne(couponDto.getPartnerId());
        CouponType couponType = couponTypeRepository.getOne(couponDto.getCouponTypeId());

        Coupon coupon = new Coupon();
        coupon.setCoupon_name(couponDto.getCouponName());
        coupon.setCoupon_desc(couponDto.getCouponDesc());
        coupon.setCoupon_price(couponDto.getCouponPrice());
        //  coupon.setCoupon_picture(couponDto.getCouponPic());
        coupon.setCoupon_expire(couponDto.getCouponExp());



        coupon.setPartner(partner);
        coupon.setCouponType(couponType);

        couponRepository.save(coupon);
        return coupon;
    }



    @Override
    public void updateCoupon(Coupon coupon) {
        couponRepository.save(coupon);
    }

    @Override
    public void deleteCoupon(String coupon) {
        couponRepository.delete(coupon);
    }

    @Override
    public Coupon getCouponById(String couponId) {

        Coupon coupon = couponRepository.findOne(couponId);

        return coupon;
    }

    @Override
    public CouponDto getcouponDTO(String couponid) {
        //TODO handshake : gambar, nama coupon, desc, date(ddmmyy),coup price

        CouponDto couponDto = new CouponDto();
        Coupon coupon = couponRepository.findOne(couponid);

        couponDto.setCouponName(coupon.getCoupon_name());
        couponDto.setCouponPrice(coupon.getCoupon_price());
        couponDto.setCouponExp(coupon.getCoupon_expire());
        couponDto.setCouponDesc(coupon.getCoupon_desc());
        couponDto.setCouponPic(coupon.getCoupon_picture());



        return couponDto;
    }

    //TODO ubah expiry date ke dalam bentuk DATE
    @Override
    public List<CouponDto> getAllCouponDto() {
        List<Coupon> coupons = couponRepository.findAll();
        List<CouponDto> couponDtos = new ArrayList<>();

        for (Coupon coupon: coupons){
            CouponDto couponDto1 = new CouponDto();

            //yang mau di show
            couponDto1.setCouponName(coupon.getCoupon_name());
            couponDto1.setCouponPrice(coupon.getCoupon_price());
            couponDto1.setCouponDesc(coupon.getCoupon_desc());
            couponDto1.setCouponExp(coupon.getCoupon_expire());

            couponDtos.add(couponDto1);



        }
        return couponDtos;
    }


    //buat nge POST image
    @Override
    public void saveImage(String couponId,MultipartFile file){
        try {
            Coupon coupon = couponRepository.findOne(couponId);

            Byte[] byteObjects = new Byte[file.getBytes().length];

            int i = 0;

            for (byte b : file.getBytes()){
                byteObjects[i++] = b;
            }

            coupon.setCoupon_picture(byteObjects);

            couponRepository.save(coupon);
        } catch (IOException e) {
            //todo handle better
            log.error("Error occurred", e);

            e.printStackTrace();
        }
    }

    @Override
    public List<Coupon> getAllData() {

        return couponRepository.findAll();
    }


//    @Override
//    public void getCoupon(String coupon) {
//        couponRepository.getCouponByCoupon_id(coupon);
//    }

    String folder = "/photos/";
//        byte[] bytes = imageFile.getBytes();
//        Path path = Paths.get(folder + imageFile.getOriginalFilename());
//        Files.write(path,bytes);
}
