package com.hackathon.rocky.service.Impl;

import com.hackathon.rocky.dao.ARLocationRepository;
import com.hackathon.rocky.dao.ARTransactionRepository;
import com.hackathon.rocky.dao.UserRepository;
import com.hackathon.rocky.dto.ARTransactionDto;
import com.hackathon.rocky.dto.UserHistoryDto;
import com.hackathon.rocky.entity.ARLocation;
import com.hackathon.rocky.entity.ARTransaction;
import com.hackathon.rocky.entity.User;
import com.hackathon.rocky.service.ARTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ARTransactionServiceImpl implements ARTransactionService {

    @Autowired
    private ARTransactionRepository arTransactionRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ARLocationRepository arLocationRepository;

    @Override
    public void  saveARTransaction(ARTransactionDto arTransactionDto) {
        User user = userRepository.getOne(arTransactionDto.getUserId());
        ARLocation arLocation = arLocationRepository.getOne(arTransactionDto.getArLocationId());

        ARTransaction arTransaction = new ARTransaction();
        arTransaction.setUser(user);
        arTransaction.setArLocation(arLocation);

        arTransactionRepository.save(arTransaction);
    }

    @Override
    public void updateARTransaction(ARTransaction arTransaction) {
        arTransactionRepository.save(arTransaction);
    }

    @Override
    public void deleteARTransaction(String arTransactionId) {
        arTransactionRepository.delete(arTransactionId);
    }

    @Override
    public List<ARTransaction> findAll() {
        return arTransactionRepository.findAll();
    }

    @Override
    public List<UserHistoryDto> getUserHistory(String userId) {
        List<UserHistoryDto> userHistoryDtos = new ArrayList<>();
        UserHistoryDto userHistoryDto = new UserHistoryDto();
        List<ARTransaction> arTransactions = arTransactionRepository.findTransactionByUserId(userId);

        for(ARTransaction transaction : arTransactions){
            userHistoryDto.setUserName(transaction.getUser().getUser_name());
            //userHistoryDto.setUserPoint(transaction.getUser().getUser_point());
            //userHistoryDto.setDate(transaction.getArTransactionTime());

            userHistoryDtos.add(userHistoryDto);
        }

        return userHistoryDtos;
    }

    @Override
    public List<ARTransaction> findAllByUserId(String userId) {
        return arTransactionRepository.findTransactionByUserId(userId);
    }

}
