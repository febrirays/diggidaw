package com.hackathon.rocky.service.Impl;

import com.hackathon.rocky.dao.PointTransactionRepository;
import com.hackathon.rocky.dto.PointTransactionDto;
import com.hackathon.rocky.entity.PointTransaction;
import com.hackathon.rocky.service.PointTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PointTransactionServiceImpl implements PointTransactionService {

    //TODO buat proses kalkulasi pas penerimaan point lewat game

    @Autowired
    private PointTransactionRepository pointTransactionRepository;

    public PointTransactionServiceImpl(PointTransactionRepository pointTransactionRepository)
    {
        this.pointTransactionRepository = pointTransactionRepository;
    }

    public void savePointTransaction(PointTransaction pointTransaction) {
        this.pointTransactionRepository.save(pointTransaction);
    }

    @Override
    public List<PointTransaction> getAllPointTransaction() {
        return pointTransactionRepository.findAll();
    }

    @Override
    public List<PointTransaction> getPointTransactionByUserId(String userID) {
        return pointTransactionRepository.getPointTransactionByUser_id(userID);
    }

    @Override
    public List<PointTransactionDto> getPointTransactionByUserId_dto(String userID) {
        List<PointTransactionDto> pointTransactionDtos = new ArrayList<>();
        PointTransactionDto pointTransactionDto = new PointTransactionDto();

        List<PointTransaction> pointTransactionList = pointTransactionRepository.getPointTransactionByUser_id(userID);

        for(PointTransaction pointTransaction : pointTransactionList)
        {
            pointTransactionDto.setUserID(pointTransaction.getUser().getUser_id());
            pointTransactionDto.setCurrentPoints(pointTransaction.getCurrentPoints());
            pointTransactionDto.setPointsChange(pointTransaction.getPointsChange());
            pointTransactionDto.setPointTransactDate(pointTransaction.getPointTransactDate());

            pointTransactionDtos.add(pointTransactionDto);
        }
        return pointTransactionDtos;
    }

}
