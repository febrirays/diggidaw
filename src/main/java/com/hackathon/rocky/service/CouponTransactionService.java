package com.hackathon.rocky.service;

import com.hackathon.rocky.dao.CouponTransactionRepository;
import com.hackathon.rocky.dto.CouponRequest;
import com.hackathon.rocky.dto.CouponTransactionDto;
import com.hackathon.rocky.dto.ResponseDTO;
import com.hackathon.rocky.dto.UserHistoryDto;
import com.hackathon.rocky.entity.CouponTransaction;

import java.util.List;

public interface CouponTransactionService {
    ResponseDTO findAllTransactionByUserID(String userId);
//  CouponTransaction getByAllTransactionByUserID(CouponTransactionDto couponTransactionDto);

    CouponTransaction saveCouponTrans(CouponRequest couponTransaction);
    void updateCouponTrans(CouponTransaction couponTransaction);
    void deleteCouponTrans (String couponTransaction);
    List<CouponTransaction> getAllData();
}