package com.hackathon.rocky.service;

import com.hackathon.rocky.dto.ARTransactionDto;
import com.hackathon.rocky.dto.UserHistoryDto;
import com.hackathon.rocky.entity.ARTransaction;

import java.util.List;

public interface ARTransactionService {
    void saveARTransaction(ARTransactionDto arTransactionDto);
    void updateARTransaction(ARTransaction arTransaction);
    void deleteARTransaction(String arTransactionId);
    List<ARTransaction> findAll();
    List<UserHistoryDto> getUserHistory(String userId);
    List<ARTransaction> findAllByUserId(String userId);

}
