package com.hackathon.rocky.service;

import com.hackathon.rocky.dto.UserDto;
import com.hackathon.rocky.entity.User;
import org.springframework.stereotype.Service;

import java.util.List;


public interface UserService  {

    void saveUser(User user);
    List<User> findAll();
}
