package com.hackathon.rocky.service;

import com.hackathon.rocky.entity.ARLocation;

public interface ARLocationService {
    void saveARLocation(ARLocation arLocation);
    void updateARLocation(ARLocation arLocation);
    void deleteARLocation(String arLocationId);
}
