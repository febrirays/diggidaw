package com.hackathon.rocky.dto;

public class CouponTransactionDto {

    private String coupon_Transaction_Id;
    private String userId;
    private String userName;
    private Integer userPoint;
    private Integer pointsReduced;
    private String couponId;
    private String couponName;

    public String getCoupon_Transaction_Id() {
        return coupon_Transaction_Id;
    }

    public void setCoupon_Transaction_Id(String coupon_Transaction_Id) {
        this.coupon_Transaction_Id = coupon_Transaction_Id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getUserPoint() {
        return userPoint;
    }

    public void setUserPoint(Integer userPoint) {
        this.userPoint = userPoint;
    }

    public Integer getPointsReduced() {
        return pointsReduced;
    }

    public void setPointsReduced(Integer pointsReduced) {
        this.pointsReduced = pointsReduced;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }
}
