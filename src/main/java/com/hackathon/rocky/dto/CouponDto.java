package com.hackathon.rocky.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;

public class CouponDto {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String couponTypeId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String partnerId;

    @NotEmpty(message = "Not null pls")
    private String couponName;

    @NotNull(message = "price cant be empty")
    private Integer couponPrice;

    @NotEmpty(message = "cant be empty")
    private String couponDesc;

    @NotEmpty(message = "cant be empty")
    private String couponExp;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Lob
    private Byte[] couponPic;

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public Integer getCouponPrice() {
        return couponPrice;
    }

    public void setCouponPrice(Integer couponPrice) {
        this.couponPrice = couponPrice;
    }

    public Byte[] getCouponPic() {
        return couponPic;
    }

    public void setCouponPic(Byte[] coupon_picture) {
        couponPic = coupon_picture;
    }

    public String getCouponDesc() {
        return couponDesc;
    }

    public void setCouponDesc(String couponDesc) {
        this.couponDesc = couponDesc;
    }

    public String getCouponExp() {
        return couponExp;
    }

    public void setCouponExp(String couponExp) {
        this.couponExp = couponExp;
    }

    public String getCouponTypeId() {
        return couponTypeId;
    }

    public void setCouponTypeId(String couponTypeId) {
        this.couponTypeId = couponTypeId;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }
}
