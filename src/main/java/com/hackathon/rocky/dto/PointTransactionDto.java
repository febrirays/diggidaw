package com.hackathon.rocky.dto;

import java.time.LocalDate;

public class PointTransactionDto {
    private String userID;

    private String PointsChange;

    private String CurrentPoints;

    private String PointTransactDate;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getPointsChange() {
        return PointsChange;
    }

    public void setPointsChange(String pointsChange) {
        PointsChange = pointsChange;
    }

    public String getCurrentPoints() {
        return CurrentPoints;
    }

    public void setCurrentPoints(String currentPoints) {
        CurrentPoints = currentPoints;
    }

    public String getPointTransactDate() {
        return PointTransactDate;
    }

    public void setPointTransactDate(String pointTransactDate) {
        PointTransactDate = pointTransactDate;
    }
}
