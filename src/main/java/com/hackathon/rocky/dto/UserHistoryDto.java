package com.hackathon.rocky.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class UserHistoryDto {
    private String transactionId;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    private String userName;
    private String date;
    private String couponName;
    private Integer pointReduced;
    private String couponDesc;


    public String getCouponDesc() {
        return couponDesc;
    }

    public void setCouponDesc(String couponDesc) {
        this.couponDesc = couponDesc;
    }

    private Byte[] couponPic;


    @JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
    public Byte[] getCouponPic() {
        return couponPic;
    }

    public void setCouponPic(Byte[] couponPic) {
        this.couponPic = couponPic;
    }

    @JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
    public String getUserName() {
        return userName;
    }


    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public Integer getPointReduced() {
        return pointReduced;
    }

    public void setPointReduced(Integer pointReduced) {
        this.pointReduced = pointReduced;
    }
}
