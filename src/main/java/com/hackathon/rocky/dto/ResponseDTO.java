package com.hackathon.rocky.dto;

import java.util.List;

public class ResponseDTO {
    private int userPoint;
    private List<UserHistoryDto> userHistories;

    public int getUserPoint() {
        return userPoint;
    }

    public void setUserPoint(int userPoint) {
        this.userPoint = userPoint;
    }

    public List<UserHistoryDto> getUserHistories() {
        return userHistories;
    }

    public void setUserHistories(List<UserHistoryDto> userHistories) {
        this.userHistories = userHistories;
    }
}
