package com.hackathon.rocky.dto;

public class ContractTransactionDto {
    public String user_name;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String user_id;
    public int total_cicilan;
    public int tenor_price;

    public int getTotal_cicilan() {
        return total_cicilan;
    }

    public void setTotal_cicilan(int total_cicilan) {
        this.total_cicilan = total_cicilan;
    }

    public int getTenor_price() {
        return tenor_price;
    }

    public void setTenor_price(int tenor_price) {
        this.tenor_price = tenor_price;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }
}
