package com.hackathon.rocky.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;


import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@Table(name="User")
@DynamicUpdate
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})


public class User implements Serializable{
    private static final long serialVersionUID = 1L;




    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name="system-uuid",strategy = "uuid")
    @Column(name = "User_id")
    private String User_id;

    @Column(name = "User_Name")
    private String User_name;

    @Column(name = "User_Point")
    private int User_point;

    //@OneToMany(mappedBy = "CouponTransaction", cascade = CascadeType.ALL)


    public String getUser_id() {
        return User_id;
    }

    public void setUser_id(String user_id) {
        User_id = user_id;
    }

    public String getUser_name() {
        return User_name;
    }

    public void setUser_name(String user_name) {
        User_name = user_name;
    }

    public int getUser_point() {
        return User_point;
    }

    public void setUser_point(int user_point) {
        User_point = user_point;
    }


}
