package com.hackathon.rocky.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "AR_Transaction")
@DynamicUpdate
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})

public class ARTransaction{
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "AR_TRANSACTION_ID")
    private String arTransactionId;

    @Column(name = "AR_TRANSACTION_TIME")
    private String arTransactionTime = LocalDate.now().toString();

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "LOCATION_ID")
    private ARLocation arLocation;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "User_id")
    private User user;

    public String getArTransactionId() {
        return arTransactionId;
    }

    public void setArTransactionId(String arTransactionId) {
        this.arTransactionId = arTransactionId;
    }

    public ARLocation getArLocation() {
        return arLocation;
    }

    public void setArLocation(ARLocation arLocation) {
        this.arLocation = arLocation;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getArTransactionTime() {
        return arTransactionTime;
    }

    public void setArTransactionTime(String arTransactionTime) {
        this.arTransactionTime = arTransactionTime;
    }
}
