package com.hackathon.rocky.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
@Entity
@Table(name = "PointTransaction")
@DynamicUpdate
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})

public class PointTransaction {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name="Point_TransactionID")
    private String PointTransactionID;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "User_id")
    private User User;

    @Column(name = "Point_TransactionDate")
    private String PointTransactDate = LocalDate.now().toString();

    @Column(name = "Point_TransactionTime")
    private String PointTransactTime = LocalTime.now().toString();

    @Column(name = "PointsChange")
    private String PointsChange;

    @Column(name ="Current_Points")
    private String CurrentPoints;

    public String getPointTransactionID() {
        return PointTransactionID;
    }

    public void setPointTransactionID(String pointTransactionID) {
        PointTransactionID = pointTransactionID;
    }

    public User getUser() {
        return User;
    }

    public void setUser(User user) {
        User = user;
    }

    public String getPointTransactDate() {
        return PointTransactDate;
    }

    public void setPointTransactDate(String pointTransactDate) {
        PointTransactDate = pointTransactDate;
    }

    public String getPointTransactTime() {
        return PointTransactTime;
    }

    public void setPointTransactTime(String pointTransactTime) {
        PointTransactTime = pointTransactTime;
    }

    public String getPointsChange() {
        return PointsChange;
    }

    public void setPointsChange(String pointsChange) {
        PointsChange = pointsChange;
    }

    public String getCurrentPoints() {
        return CurrentPoints;
    }

    public void setCurrentPoints(String currentPoints) {
        CurrentPoints = currentPoints;
    }
}
