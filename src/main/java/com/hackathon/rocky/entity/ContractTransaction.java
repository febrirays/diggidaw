package com.hackathon.rocky.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;

@Data
@Entity
@Table(name = "Contract_Transaction")
@DynamicUpdate
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class ContractTransaction {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name="system-uuid",strategy="uuid")
    @Column(name = "Contract_Transaction_id")
    private String contract_transaction_id;

    @Column(name = "Total_Cicilan")
    private int total_cicilan;

    @Column(name = "Tenor_Price")
    private int tenor_price;

    public void setTenor_price(int tenor_price) {
        this.tenor_price = tenor_price;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_Id")
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    public String getContract_transaction_id() {
        return contract_transaction_id;
    }

    public void setContract_transaction_id(String contract_transaction_id) {
        this.contract_transaction_id = contract_transaction_id;
    }

    public int getTotal_cicilan() {
        return total_cicilan;
    }

    public void setTotal_cicilan(int total_cicilan) {
        this.total_cicilan = total_cicilan;
    }

    public int getTenor_price() {
        return tenor_price;
    }


}
