package com.hackathon.rocky.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "AR_Location")
@DynamicUpdate
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})

public class ARLocation{
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "LOCATION_ID")
    private String locationId;

    @Column(name = "LOCATION_NAME")
    private String locationName;

    @Column(name = "LOCATION_ADDRESS")
    private String locationAddress;

    @Column(name = "LOCATION_PICTURE")
    private String locationPicture;

    @Column(name = "LONGITUDE")
    private Double longitude;

    @Column(name = "LATITUDE")
    private Double latitude;

    /*@OneToMany(fetch = FetchType.LAZY, mappedBy = "arLocation")
    private List<ARTransaction> arTransactions;*/

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getLocationAddress() {
        return locationAddress;
    }

    public void setLocationAddress(String locationAddress) {
        this.locationAddress = locationAddress;
    }

    public String getLocationPicture() {
        return locationPicture;
    }

    public void setLocationPicture(String locationPicture) {
        this.locationPicture = locationPicture;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    /*public List<ARTransaction> getArTransactions() {
        return arTransactions;
    }

    public void setArTransactions(List<ARTransaction> arTransactions) {
        this.arTransactions = arTransactions;
    }*/
}
