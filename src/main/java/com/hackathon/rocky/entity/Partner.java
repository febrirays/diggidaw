package com.hackathon.rocky.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;


@Entity
@Table(name = "Partner")
@DynamicUpdate

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})


public class Partner {
    private static final long serialVersionUID = 1L;


    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "Partner_id")
    private String partner_id;

    @NotEmpty(message = "pls no null")
    @Column(name = "Partner_name")
    private String Partner_name;
    @NotEmpty(message = "Address cant be empty")
    @Column(name = "Partner_address")
    private String Partner_address;

    @NotEmpty(message = "description cant be empty")
    @Column(name = "Partner_description")
    private String Partner_description;

//    @OneToMany(mappedBy = "partner")
//    private List<Coupon> Coupon;

    public String getPartner_id() {
        return partner_id;
    }

    public void setPartner_id(String partner_id) {
        this.partner_id = partner_id;
    }

    public String getPartner_name() {
        return Partner_name;
    }

    public void setPartner_name(String partner_name) {
        Partner_name = partner_name;
    }

    public String getPartner_address() {
        return Partner_address;
    }

    public void setPartner_address(String partner_address) {
        Partner_address = partner_address;
    }

    public String getPartner_description() {
        return Partner_description;
    }

    public void setPartner_description(String partner_description) {
        Partner_description = partner_description;
    }

//    public List<com.hackathon.rocky.entity.Coupon> getCoupon() {
//        return Coupon;
//    }
//
//    public void setCoupon(List<com.hackathon.rocky.entity.Coupon> coupon) {
//        Coupon = coupon;
//    }
}

