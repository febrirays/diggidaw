package com.hackathon.rocky.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.hackathon.rocky.Sequence.generatorId;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.persister.walking.internal.FetchStrategyHelper;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDateTime;


@Data
@Entity
@Table(name = "Coupon")
@DynamicUpdate
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})

public class Coupon implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "book_seq")
    @GenericGenerator(
            name = "book_seq",
            strategy = "com.hackathon.rocky.Sequence.generatorId" +
                    "",
            parameters = {
                    @Parameter(name = generatorId.INCREMENT_PARAM, value = "50"),
                    @Parameter(name = generatorId.VALUE_PREFIX_PARAMETER, value = "CPN_"),
                    @Parameter(name = generatorId.NUMBER_FORMAT_PARAMETER, value = "%05d")})

    @Column(name = "Coupon_id")
    private String Coupon_id;

    //@NotEmpty(message = "Please provide a name")
    @Column(name = "Coupon_name")
    private String Coupon_name;

    //@NotNull(message = "Please provide price")
    @DecimalMin("1.00")
    @Column(name = "Coupon_price")
    private Integer Coupon_price;

    public Byte[] getCoupon_picture() {
        return Coupon_picture;
    }

    public void setCoupon_picture(Byte[] coupon_picture) {
        Coupon_picture = coupon_picture;
    }

    @Lob
    private Byte[] Coupon_picture;

    // @NotEmpty(message = "Please provide description")
    @Column(name = "Coupon_desc")
    private String Coupon_desc;


    @Column(name = "Coupon_created")
    private String Coupon_created = LocalDateTime.now().toString();

    @Column(name = "Coupon_expire")
    private String Coupon_expire;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "Coupon_type_id")
    private CouponType CouponType;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "Partner_id")
    private Partner Partner;
}
    //@OneToMany(mappedBy = "CouponTransaction", cascade = CascadeType.ALL)

