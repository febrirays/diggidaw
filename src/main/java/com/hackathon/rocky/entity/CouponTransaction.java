package com.hackathon.rocky.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDateTime;


@Entity
@Table(name = "Coupon_Transaction")
@DynamicUpdate
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})

public class CouponTransaction implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name="Coupon_trans_id")
    private String Coupon_trans_id;

    @Column(name = "Trans_time")
    private String Coupon_transtime = LocalDateTime.now().toString();

    @Column(name = "Points_reduced")
    private Integer Points_reduced;

    @Column(name = "Current_Point")
    private Integer Current_point;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "Coupon_id")
    private Coupon coupon;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "User_id")
    private User user;

    public String getCoupon_trans_id() {
        return Coupon_trans_id;
    }

    public void setCoupon_trans_id(String coupon_trans_id) {
        Coupon_trans_id = coupon_trans_id;
    }

    public String getCoupon_transtime() {
        return Coupon_transtime;
    }

    public void setCoupon_transtime(String coupon_transtime) {
        Coupon_transtime = coupon_transtime;
    }

    public Integer getPoints_reduced() {
        return Points_reduced;
    }

    public void setPoints_reduced(Integer points_reduced) {
        Points_reduced = points_reduced;
    }

    public Integer getCurrent_point() {
        return Current_point;
    }



    public void setCurrent_point(Integer current_point) {
        Current_point = current_point;
    }

    public Coupon getCoupon() {
        return coupon;
    }

    public void setCoupon(Coupon coupon) {
        this.coupon = coupon;
    }

//    public User getUserPoint(Integer userPoint){
//        return getUserPoint(userPoint);
//    }

    public Coupon getCouponPrice(Integer price){


        return getCouponPrice(price);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


}
