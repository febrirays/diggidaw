package com.hackathon.rocky.dao;

import com.hackathon.rocky.entity.PointTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface PointTransactionRepository extends JpaRepository<PointTransaction,String> {
    @Query(value = "SELECT * FROM point_transaction WHERE user_id LIKE :userID",nativeQuery = true)
    List<PointTransaction> getPointTransactionByUser_id(@Param("userID") String userID);
}
