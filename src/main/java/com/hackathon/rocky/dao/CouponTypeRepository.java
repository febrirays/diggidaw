package com.hackathon.rocky.dao;


import com.hackathon.rocky.entity.CouponType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface CouponTypeRepository extends JpaRepository<CouponType, String> {

}
