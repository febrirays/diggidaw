package com.hackathon.rocky.dao;

import com.hackathon.rocky.entity.ARLocation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public interface ARLocationRepository extends JpaRepository<ARLocation, String> {

}
