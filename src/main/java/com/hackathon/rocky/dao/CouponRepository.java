package com.hackathon.rocky.dao;

import com.hackathon.rocky.entity.Coupon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface CouponRepository extends JpaRepository<Coupon,String> {
//    Coupon getCouponByCoupon_id(String id);

}
