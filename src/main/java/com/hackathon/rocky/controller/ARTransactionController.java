package com.hackathon.rocky.controller;

import com.hackathon.rocky.dto.ARTransactionDto;
import com.hackathon.rocky.dto.UserHistoryDto;
import com.hackathon.rocky.entity.ARTransaction;
import com.hackathon.rocky.service.ARTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/ar-transaction")
public class ARTransactionController {

    @Autowired
    private ARTransactionService arTransactionService;

        @PostMapping("/save-ar-transaction")
    public ResponseEntity<?> saveARTransaction(@RequestBody ARTransactionDto arTransactionDto){
        arTransactionService.saveARTransaction(arTransactionDto);
        return new ResponseEntity(arTransactionDto, HttpStatus.OK);
    }

    @PostMapping("/update-ar-transaction")
    public ResponseEntity<?> updateARTransaction(@RequestBody ARTransaction arTransaction){
        arTransactionService.updateARTransaction(arTransaction);
        return new ResponseEntity(arTransaction, HttpStatus.OK);
    }

    @GetMapping("/delete-ar-transaction/{arTransactionId}")
    public ResponseEntity<?> deleteContract(@PathVariable String arTransactionId){
        arTransactionService.deleteARTransaction(arTransactionId);
        return new ResponseEntity(arTransactionId, HttpStatus.OK);
    }

    @GetMapping("/get-all-transaction")
    public ResponseEntity<?> getAllTransaction(){
        List<ARTransaction> arTransactions = arTransactionService.findAll();
        return new ResponseEntity(arTransactions, HttpStatus.OK);
    }

    /*@GetMapping("/get-transaction/{arTransactionId}")
    public ResponseEntity<?> getTransactionById(@PathVariable String arTransactionId){
        ARTransaction arTransaction = arTransactionService.findById(arTransactionId);
        return new ResponseEntity(arTransaction, HttpStatus.OK);
    }*/
    @GetMapping("/get-user-ar-transaction-history/{userId}")
    public ResponseEntity<?> getUserARTransactionHistory(@PathVariable String userId){
        List<UserHistoryDto> userHistoryDtos = arTransactionService.getUserHistory(userId);
        return new ResponseEntity(userHistoryDtos, HttpStatus.OK);
    }

    @GetMapping("/get-all-transaction/{userId}")
    public ResponseEntity<?> getAllTransactionByUserId(@PathVariable String userId){
        List<ARTransaction> arTransactions = arTransactionService.findAllByUserId(userId);
        return new ResponseEntity(arTransactions, HttpStatus.OK);
    }
}
