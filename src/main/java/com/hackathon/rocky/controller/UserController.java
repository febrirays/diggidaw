package com.hackathon.rocky.controller;

import com.hackathon.rocky.dao.UserRepository;
import com.hackathon.rocky.dto.UserDto;
import com.hackathon.rocky.entity.User;
import com.hackathon.rocky.service.UserService;
import jdk.nashorn.internal.parser.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.CascadeType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    private UserRepository userRepository;

    @GetMapping("/view")
    public ResponseEntity<?> viewUser(){

//        user1.setUser_id(user1.getUser_id());
//        userService.findAll(user1);
        List<User> all = userService.findAll();
        return new ResponseEntity(all, HttpStatus.OK);
    }

    @PostMapping("/save-user")
    public ResponseEntity<?> saveUser(@RequestBody User user){

        userService.saveUser(user);

        return new ResponseEntity(user,HttpStatus.OK);
    }



}
