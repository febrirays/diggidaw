package com.hackathon.rocky.controller;

import com.hackathon.rocky.dto.PointTransactionDto;
import com.hackathon.rocky.entity.PointTransaction;
import com.hackathon.rocky.service.PointTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/point_transaction")
public class PointTransactionController {
    @Autowired
    private PointTransactionService pointTransactionService;

    @PostMapping(value = "/save")
    public ResponseEntity<?> savePointTransaction(@RequestBody PointTransaction pointTransaction)
    {
        PointTransaction pointTransaction1 = new PointTransaction();
        pointTransaction1.setPointsChange("200");
        pointTransaction1.setPointTransactDate("18/07/1997");

        pointTransactionService.savePointTransaction(pointTransaction);
        return new ResponseEntity<>(pointTransaction, HttpStatus.OK);
    }
    @GetMapping(value = "/getAllPointTransaction") //satu fungsi untuk mengirimkan semua data PT
    public ResponseEntity<?> getAllPointTransaction() {
        List<PointTransaction> pointTransactionList=pointTransactionService.getAllPointTransaction();

        return new ResponseEntity<>(pointTransactionList,HttpStatus.OK);
    }

    @GetMapping(value = "/getPointTransaction/{userID}")
    public ResponseEntity<?> getPointTransactionByUserID(@PathVariable String userID)
    {
        List<PointTransaction> pointTransaction = pointTransactionService.getPointTransactionByUserId(userID);

        return new ResponseEntity<>(pointTransaction,HttpStatus.OK);
    }

    @GetMapping(value = "/getPointTransaction_dto/{userID}")
    public ResponseEntity<?> getPointTransactionDTOByUserID(@PathVariable String userID)
    {
        List<PointTransactionDto> pointTransactionDtoList = pointTransactionService.getPointTransactionByUserId_dto(userID);

        return new ResponseEntity<>(pointTransactionDtoList,HttpStatus.OK);
    }
}
