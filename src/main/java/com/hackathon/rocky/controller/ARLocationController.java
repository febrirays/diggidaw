package com.hackathon.rocky.controller;

import com.hackathon.rocky.entity.ARLocation;
import com.hackathon.rocky.service.ARLocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/v1/ar-location")
public class ARLocationController {

    @Autowired
    private ARLocationService arLocationService;

    @PostMapping("/save-ar-location")
    public ResponseEntity<?> saveARLocation(@RequestBody ARLocation arLocation){
        arLocationService.saveARLocation(arLocation);
        return new ResponseEntity(arLocation, HttpStatus.OK);
    }

    @PostMapping("/update-ar-location")
    public ResponseEntity<?> updateARLocation(@RequestBody ARLocation arLocation){
        arLocationService.updateARLocation(arLocation);
        return new ResponseEntity(arLocation, HttpStatus.OK);
    }

    @GetMapping("/delete-ar-location/{arLocationId}")
    public ResponseEntity<?> deleteARLocation(@PathVariable String arLocationId){
        arLocationService.deleteARLocation(arLocationId);
        return new ResponseEntity(arLocationId, HttpStatus.OK);
    }
}
