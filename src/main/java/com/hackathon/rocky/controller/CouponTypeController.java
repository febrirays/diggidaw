package com.hackathon.rocky.controller;


import com.hackathon.rocky.entity.CouponType;
import com.hackathon.rocky.service.CouponTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/couponType")
public class CouponTypeController {

    @Autowired
    private CouponTypeService couponTypeService;

    @PostMapping("/save-couponType" )
    public ResponseEntity<?> saveCoupon(@RequestBody CouponType couponType){

        // do validation

        CouponType couponType1= new CouponType();
        couponType1.setCoupon_type_name("jonny sins");

        // save contract data
        couponTypeService.saveCouponType(couponType);

        return new ResponseEntity(couponType, HttpStatus.OK);
    }

    @PostMapping("/update-couponType")
    public ResponseEntity<?> updateCoupon(@RequestBody CouponType couponType){

        // do validation

        // save contract data
        couponTypeService.UpdateCouponType(couponType);

        return new ResponseEntity(couponType, HttpStatus.OK);
    }

    @GetMapping("/delete-couponType/{CouponTypeId}")
    public ResponseEntity<?> deleteCouponType(@PathVariable String CouponTypeId){

        // do validation

        // save contract data
        couponTypeService.DeleteCouponType(CouponTypeId);

        return new ResponseEntity(CouponTypeId, HttpStatus.OK);
    }


}
