package com.hackathon.rocky.controller;

import com.hackathon.rocky.dao.CouponTransactionRepository;
import com.hackathon.rocky.dto.CouponRequest;
import com.hackathon.rocky.dto.UserHistoryDto;
import com.hackathon.rocky.entity.Coupon;
import com.hackathon.rocky.service.CouponTransactionService;
import com.hackathon.rocky.entity.CouponTransaction;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.aspectj.weaver.ast.Var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@RestController
@RequestMapping(value = "/couponTrans")
public class CouponTransController {


    @Autowired
    private CouponTransactionService couponTransactionService;

    //TODO nge get gambar
    @PostMapping("/save-couponTrans")
    public ResponseEntity<?> saveCouponTrans(@RequestBody CouponRequest couponTrans) {

//        Coupon coupon1 = new Coupon();
//        coupon1.setCoupon_name("p ainis");
//        coupon1.setCoupon_desc("24234234");

        // save contract data
        CouponTransaction couponTransaction = couponTransactionService.saveCouponTrans(couponTrans);

        return new ResponseEntity(couponTransaction, HttpStatus.OK);
    }


    //Ambil data transaksi kupon user
    @CrossOrigin
    @GetMapping("/get-userHistory/{userId}")
    public ResponseEntity<?> getUserHistory(@PathVariable String userId) {
        //service


        //List<CouponTransaction> userHistory = couponTransactionService.findAllTransactionByUserID(userId);
       // CouponTransaction userHistory = couponTransactionService.UserHistory(couponRequest);

//        List<UserHistoryDto> userHistoryDtos = );
        return new ResponseEntity(couponTransactionService.findAllTransactionByUserID(userId), HttpStatus.OK);
    }

    //Show ALL Transaction data
    @GetMapping("/get-allCouponTrans")
    public ResponseEntity<?> getCoupon() {
        //service

        List<CouponTransaction> allData = couponTransactionService.getAllData();
        return new ResponseEntity(allData, HttpStatus.OK);
    }


    //TODO buat database BLOB buat nampilin gambar
//    @RequestMapping(value = "/gambar", method = RequestMethod.GET,
//            produces = MediaType.IMAGE_JPEG_VALUE)
//    public ResponseEntity<InputStreamResource> getImage() throws IOException {
//
//        ClassPathResource imgFile = new ClassPathResource("image/pope.jpg");
//
//
//        return ResponseEntity
//                .ok()
//                .contentType(MediaType.IMAGE_JPEG)
//                .body(new InputStreamResource(imgFile.getInputStream()));
//    }






//     return new ResponseEntity(??, HttpStatus.OK);
//     return new ResponseEntity(??, HttpStatus.OK);


//    @GetMapping("/get-coupon/{CouponId}")
//    public ResponseEntity<?> getCoupon(@PathVariable String Couponid){
//
//        // do validation
//
//        Coupon coupon1 = new Coupon();
//        coupon1.getCoupon_name();
//        coupon1.getCouponType().getCoupon_type_name();
//        coupon1.getPartner().getPartner_name();
//
//        // save contract data
//
//        return  coupon1;
//    }

    @PostMapping("/update-couponTrans")
    public ResponseEntity<?> updateCouponTrans(@RequestBody CouponTransaction couponTrans) {

        // do validation

        // save contract data
        couponTransactionService.updateCouponTrans(couponTrans);

        return new ResponseEntity(couponTrans, HttpStatus.OK);
    }

    @GetMapping("/post-contractTrans/")
    public ResponseEntity<?> simpan(@PathVariable String CouponId, @PathVariable String UserId) {

        return new ResponseEntity(CouponId, HttpStatus.OK);

    }

    @GetMapping("/delete-contractTrans/{CouponTransId}")
    public ResponseEntity<?> deleteCoupon(@PathVariable String CouponTransId) {
        couponTransactionService.deleteCouponTrans(CouponTransId);
        return new ResponseEntity(CouponTransId, HttpStatus.OK);
    }


}



//    @RequestMapping(value = "/get/{couponTransaction}", method = RequestMethod.GET)
//
//    public String showBookDetails(@PathVariable String couponTransaction, Model model) {
//
//        model.addAttribute("couponTransaction", couponTransaction);
//
//
//        return new CouponTransactionRepository(couponTransaction);