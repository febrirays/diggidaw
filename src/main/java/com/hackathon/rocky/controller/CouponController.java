package com.hackathon.rocky.controller;

import com.hackathon.rocky.dao.CouponRepository;
import com.hackathon.rocky.dto.CouponDto;
import com.hackathon.rocky.entity.Coupon;
import com.hackathon.rocky.service.CouponService;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/coupon")
@Validated
public class CouponController {

    Logger log = LoggerFactory.getLogger(CouponController.class);

    @Autowired
    private CouponService couponService;



    //save
    @PostMapping("/save-coupon")
    @ResponseStatus(HttpStatus.CREATED)

    public ResponseEntity<?> saveCoupon(@Valid @RequestBody CouponDto couponDto){

        Coupon coupon = couponService.saveCoupon(couponDto);

        return new ResponseEntity(coupon, HttpStatus.OK);

    }



    //find 1
    @GetMapping("/get-coupon/{couponId}")
    public ResponseEntity<?> getCoupon(@Validated @NotBlank @PathVariable String couponId) {
        //service


        //List<CouponTransaction> userHistory = couponTransactionService.findAllTransactionByUserID(userId);
        // CouponTransaction userHistory = couponTransactionService.UserHistory(couponRequest);

       //Coupon coupon= couponService.getCouponById(couponId);
        return new ResponseEntity(couponService.getcouponDTO(couponId), HttpStatus.OK);
    }

    @GetMapping("/getAllCouponDto")
    public ResponseEntity<?> getUserHistory() {

        List<CouponDto> allCoupon = couponService.getAllCouponDto();

        return new ResponseEntity(allCoupon, HttpStatus.OK);
    }

    @GetMapping("/get-AllCoupons")
    public ResponseEntity<?> getCoupon() {
        //service

        List<Coupon> allData = couponService.getAllData();
        return new ResponseEntity(allData, HttpStatus.OK);
    }



    //TODO buat sistem supaya hanya bisa search yang dia punya DAN setelah login --> Authorization
        //get image stored as BLOB
    @GetMapping("/get-couponBlob/{couponId}")
    public ResponseEntity<?> couponImageBlob(@PathVariable String couponId) {
        Coupon coupon = couponService.getCouponById(couponId);
        if (coupon.getCoupon_picture() == null   ) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
           //return ResponseEntity.
        }

        return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, "image/*").body(coupon.getCoupon_picture());
   }



    //get image already rendered
    @GetMapping("/{couponid}/couponImage")
    public void renderImageFromDB(@PathVariable String couponid, HttpServletResponse response) throws IOException {
        Coupon coupon = couponService.getCouponById(couponid);

        if (coupon.getCoupon_picture() != null) {
            byte[] byteArray = new byte[coupon.getCoupon_picture().length];
            int i = 0;

            for (Byte wrappedByte : coupon.getCoupon_picture()){
                byteArray[i++] = wrappedByte; //auto unboxing
            }

            response.setContentType("image/jpeg");
            InputStream is = new ByteArrayInputStream(byteArray);
            StreamUtils.copy(is, response.getOutputStream());
        }
    }



    //update
    @PostMapping("/update-coupon")
    public ResponseEntity<?> updateCoupon(@Valid @RequestBody Coupon coupon){

        // do validation

        // save contract data
        couponService.updateCoupon(coupon);

        return new ResponseEntity(coupon, HttpStatus.OK);
    }



    //delete
    @GetMapping("/delete-contract/{couponId}")
    public ResponseEntity<?> deleteCoupon(@PathVariable String couponid){
        couponService.deleteCoupon(couponid);

        return new ResponseEntity(couponid, HttpStatus.OK);
    }

//    String returnValue = "start";
//        try {
//        couponService.saveImage(imageFile);
//    } catch (Exception e) {
//        e.printStackTrace();
//        log.error("Error saving photo",e);
//        returnValue = "error";
//    }
//
//        return returnValue;

    //save image
    @PostMapping("/save-image/{id}")
    public String saveImage(@PathVariable String id, @RequestParam("imagefile") MultipartFile file){

        couponService.saveImage(String.valueOf(id), file);

        return "success";

    }



}
